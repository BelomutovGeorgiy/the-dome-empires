import { TestBed, inject } from '@angular/core/testing';

import { EmpireResourcesService } from './empire-resources.service';

describe('EmpireResourcesServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmpireResourcesService]
    });
  });

  it('should be created', inject([EmpireResourcesService], (service: EmpireResourcesService) => {
    expect(service).toBeTruthy();
  }));
});
