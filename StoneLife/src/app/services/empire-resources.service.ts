import { Injectable } from '@angular/core';
import { EmpireResources } from "../models/empire-resources";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of';

@Injectable()
export class EmpireResourcesService {

  private resources: EmpireResources;

  constructor() { 
    this.resources ={
      Energy:70,
      GroundForces:50,
      SpaceForces:50,
      Respect:25
    }
  }

  public getEmpireResources(): Observable<EmpireResources>{
    return Observable.of(this.resources);
  }

  public assignInfluence(resources: EmpireResources){
    this.resources.Energy+=resources.Energy;
    this.resources.GroundForces+=resources.GroundForces;
    this.resources.SpaceForces+=resources.SpaceForces;
    this.resources.Respect+=resources.Respect;
  }
}
