import { Component, OnInit } from '@angular/core';
import { EmpireResourcesService } from "./services/empire-resources.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

  providers:[EmpireResourcesService]
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
  }
  title = 'app';

  constructor(private empireResourcesService: EmpireResourcesService ) {
      }
}
