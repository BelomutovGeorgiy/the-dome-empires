import { Component, OnInit } from '@angular/core';
import { EmpireResources } from "../models/empire-resources";
import { EmpireResourcesService } from "../services/empire-resources.service";

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss']
})
export class ParametersComponent implements OnInit {

  private resources: EmpireResources;

  constructor(private empireResourcesService: EmpireResourcesService ) {
    this.empireResourcesService.getEmpireResources().subscribe((resources: EmpireResources) =>{this.resources=resources})
   }

  ngOnInit() {
  }

}
