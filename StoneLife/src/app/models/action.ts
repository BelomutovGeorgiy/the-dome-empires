import { EmpireResources } from "./empire-resources";

export interface Action {
    Name:string;
    Image: string;
    Description:string;
    AcceptInfluence: EmpireResources;
    DeclineInfluence: EmpireResources;
    NextAction?: Action;
}
