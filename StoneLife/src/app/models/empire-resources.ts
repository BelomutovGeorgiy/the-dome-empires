export class EmpireResources {
    public Energy: number;
    public SpaceForces: number;
    public GroundForces: number;
    public Respect: number;
}
