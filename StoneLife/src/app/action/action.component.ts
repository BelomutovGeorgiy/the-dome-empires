import { Component, OnInit } from '@angular/core';
import { Action } from "../models/action";
import { EmpireResourcesService } from "../services/empire-resources.service";

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionComponent implements OnInit {

  private actions: Action[];

  private currentAction: Action;
  constructor(private empireResourcesService: EmpireResourcesService ) { }

  ngOnInit() {
    this.actions = [
      <Action>{
        Name:"Invasion!",
        Description:"We got a couple thrid-human's freighter near solar system. We think we must prepare for space invasion",
        Image:"http://coolvibe.com/wp-content/uploads/2010/12/convergence_by_togman_studio-d34fs6i.jpg",
        AcceptInfluence:{
          Respect:5,
          GroundForces:0,
          SpaceForces:-10,
          Energy:-2
        },
        DeclineInfluence:{
          Respect:-10,
          GroundForces:0,
          SpaceForces:0,
          Energy:2
        }
      },
      <Action>{
        Name:"Intervention!",
        Description:"They broke our defence, Prepare for heavy intervention!!!",
        Image:"https://datafist.files.wordpress.com/2013/01/dust514logo.jpg",
        AcceptInfluence:{
          Respect:8,
          GroundForces:-10,
          SpaceForces:0,
          Energy:-2
        },
        DeclineInfluence:{
          Respect:-25,
          GroundForces:-5,
          SpaceForces:0,
          Energy:2
        }
      },
      <Action>{
        Name:"Crime",
        Description:"Criminal syndicate grow up in one of our systems. They may broke our economy, shall we destroy them?",
        Image:"https://cdnb.artstation.com/p/assets/images/images/001/745/563/large/richard-roberts-cyberpunk.jpg?1452114862",
        AcceptInfluence:{
          Respect:-5,
          GroundForces:-5,
          SpaceForces:-1,
          Energy:10
        },
        DeclineInfluence:{
          Respect:0,
          GroundForces:0,
          SpaceForces:0,
          Energy:-10
        }
      },
      
    ]
    this.changeAction();
  }

  changeAction(){
    this.currentAction = this.actions[Math.floor(Math.random() * this.actions.length)]
  }

  confirmAction(){
    this.empireResourcesService.assignInfluence(this.currentAction.AcceptInfluence);
    
    this.changeAction();
  }
  declineAction(){
    this.empireResourcesService.assignInfluence(this.currentAction.DeclineInfluence);

    this.changeAction();
  }

}
